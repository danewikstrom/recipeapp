//
//  NextViewController.swift
//  Recipes
//
//  Created by Dane Wikstrom on 9/29/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit

class NextViewController: UIViewController, UITextViewDelegate  {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var ingredientsTextView: UITextView!
    @IBOutlet weak var directionsTextView: UITextView!
    
    
    var info: Dictionary<String, AnyObject>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        nameLabel.text = info!["Name"] as? String//set label
        
        let url = URL(string: (info!["Picture"] as? String)!)//get the url
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                self.recipeImage.image = UIImage(data: data!)//set image
            }
        }
        
        ingredientsTextView.text = info!["Ingredients"] as? String//set ingredients in textview
        directionsTextView.text = info!["Directions"] as? String//set directions in textview
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
